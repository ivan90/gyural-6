<?php

/**
 * Gyural > 3rd Funcs > FileSystem
 *
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

function filesystem__rrmdir($dir) { 
	if (is_dir($dir)) { 
		$objects = scandir($dir); 
		foreach ($objects as $object) { 
			if ($object != "." && $object != "..") { 
				if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object); 
			}
		}
		reset($objects); 
		rmdir($dir); 
	}
}

// @ Andrea Rufo - Slug
function filesystem__slug($file, $maxLength = 50) {
	
	$info = pathinfo($file['name']);
	
	$basename = $info['basename']; 
	$ext = $info['extension'];
 
	$result = CallFunction('strings', 'slug', $basename);
	$result = date('U').'-'.$result.'.'.$ext;
	
	return $result;
 
}
