<?php

/**
 * Gyural > Funcs > Ctrls
 * Controllers
 *
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

/**
 * Alias of LoadController
 * 
 * @param string $lib
 * @param boolean $instance
 * @param array $args
 * @return standardController
 */
function LoadApp($lib, $istance = false, $args = null) {
	return LoadController($lib, $istance, $args);
}

/**
 * Check if a $page is an app controllers.
 * 
 * @param string $page
 */
function IsController($page) {
	
	$parts = explode('/', $page);
	foreach($parts as $v)
		if(strlen($v) > 0)
			$pts[] = $v;
			
	$Controller = $pts[0];
	
	if(HaveControllers($Controller)) {
		
		$ControllerApp = LoadController($Controller, 1);
		
		while(count($pts) > 1) {
			
			$suitable = $ControllerApp->__haveController($pts);
			if($suitable)
				return $suitable;
			
			unset($pts[count($pts)-1]);
			
		}
		
		if($ControllerApp->index_tollerant == true) {
			$parts[] = 'index';
			$suitable = $ControllerApp->__haveController($pts);
		}
		
		if($suitable)
			return $suitable;
		else
			return false;
		
	}

	return false;
}


/**
 * Load a Controller.
 * 
 * @param string $lib
 * @param boolean $create
 * @param array $args
 * @return standardController
 */
function LoadController($Controller, $create = false, $args = null) {
	
	if(strstr($Controller, 'Ctrl'))
		$Controller = str_replace('Ctrl', '', $Controller);
	
	$whereToLook = HaveControllers($Controller);
	if($whereToLook) {
		include_once $whereToLook;
		$Controller .= 'Ctrl';
	
		if($create == 1) {
			if($args != null)
				return new $Controller($args);
			else
				return new $Controller;
		}
		else
			return true;
		
	}

	return false;
	
}

/**
 * Check if $controllerName is a controller.
 * 
 * @param string $controllerName
 * @return string File path
 */
function HaveControllers($controllerName) {
	
	$portions = explode('_', $controllerName);
	$counter = count($portions);
	$controllerDir = $controllerName;

	while($counter > 0) {

		unset($portions[$counter]);
		$controllerDir = implode('_', $portions);

		if(strlen($controllerDir) == 0)
			return false;
		
		if($controllerDir[0] != '*') {
			if(legacy == 0) {
				$specific = application . $controllerDir . DIRECTORY_SEPARATOR . '_' . DIRECTORY_SEPARATOR . $controllerName.'.ctrl.php';
				if(!is_file($specific))
					$specific = applicationCore . $controllerDir . DIRECTORY_SEPARATOR . '_' . DIRECTORY_SEPARATOR . $controllerName.'.ctrl.php';
			} else {
				$specific = application . $controllerDir . DIRECTORY_SEPARATOR . '_' . DIRECTORY_SEPARATOR . $controllerName.'.ctrl.php';
			}
		} else {
			$controllerName = str_replace('*', '', $controllerName);
			$specific = libs . $controllerName . ".ctrl.php";
		}
		
		$global = libs . $controllerName . ".ctrl.php";

		if(is_file($specific))
			return $specific;
		else if(is_file($global))
			return $global;

		$counter--;

	}

	return false;
	
}
