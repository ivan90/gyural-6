<?php

\Gyu\Hooks::set('gyu.db.ready', 'users_hook__login');
\Gyu\Hooks::set('gyu.after-libs', 'users_hook__init');

function users_hook__init($time = null) {
	LoadClass('users');
}

function users_hook__login($time = null) {
	
	$tried = false;
	
	if(isset($_POST["username"], $_POST["password"])) {
			
		$username = $_POST["username"];
		$password = $_POST["password"];

		if($user = LoadClass('users', 1)->_try($username, $password))
			$_SESSION["login"] = $user;
		else
			unset($_SESSION["login"]);

		$tried = true;

	} else if(@is_object($_SESSION["login"])) {
		
		if(!$_SESSION["login"]->_refresh())
			unset($_SESSION["login"]);

		$tried = true;

	}
	
	if($tried && !logged()) {
		$wrongUrl = $_POST["uriLogin"] ? $_POST["uriLogin"] : uriLogin;
		$a = LoadClass('standardController', 1);
		
		$a->move($wrongUrl, array('error' => 'Wrong Username or password.'));
		return false;
	}

	if(logged())
		Me()->_ping();

}

?>