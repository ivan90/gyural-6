<?php

/*

----------
{{app}} {{ctrl.clean}}
----------

Filename: /app/{{app}}/_/{{ctrl}}.hooks.php
 Version: 0.1
  Author: {{user}} <{{email}}>
    Date: {{time}}
	
*/

\Gyu\Hooks::set('gyu.created', '{{ctrl}}_sample_hook');

function {{ctrl}}_sample_hook($time) {

	// this is a sample hook!

}

?>