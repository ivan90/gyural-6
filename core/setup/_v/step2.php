<?
if(isset($app_data["error"])) {	
	?>
	<div class="alert-box alert">
		<?
			if($app_data["error"]["uploadPass"] == null)
				echo '<p style="margin: 0">La cartella per gli upload non è scrivibile.</p>';
			
			if($app_data["error"]["mysqlPass"] != null)
				echo '<p style="margin: 0">Il database ha risposto: ' . $app_data["error"]["mysqlPass"] . '</p>';
		?>
	</div>
	<?
}
?>
<h3 class="subs">Configurazione</h3>
<hr />
<form method="post" action="/setup/2">
	<h4>Informazioni Generali</h4>
	<input type="hidden" name="ssl" value="0" />
	<input type="hidden" name="dev" value="0" />
	<input type="hidden" name="logStack" value="0" />
	<input type="hidden" name="sendmail" value="php" />

	<div class="row collapse">
		<div class="medium-4 column">
			<span class="prefix">Nome del Sito</span>
		</div>
		<div class="medium-8 column">
			<input required="true" placeholder="Nome del Sito" type="text" name="siteName" value="<? echo $_SESSION["prov"]->siteName; ?>" />
		</div>
	</div>

	<div class="row collapse">
		<div class="medium-4 column">
			<span class="prefix">URI</span>
		</div>
		<div class="medium-8 column">
			<input required="true" placeholder="URI" type="text" name="uri" value="<? echo isset($_SESSION["prov"]->uri) ? $_SESSION["prov"]->uri : 'http://' . $_SERVER["HTTP_HOST"]; ?>" />
		</div>
	</div>

	<div class="row collapse">
		<div class="medium-4 column">
			<span class="prefix">Cartella Upload</span>
		</div>
		<div class="medium-8 column">
			<input required="true" placeholder="Cartella upload" type="text" name="upl" value="<? echo isset($_SESSION["prov"]->uploadPath) ? $_SESSION["prov"]->uploadPath : str_replace(absolute, '', upload); ?>" />
		</div>
	</div>

	<div class="row collapse">
		<div class="medium-4 column">
			<span class="prefix">Email</span>
		</div>
		<div class="medium-8 column">
			<input required="true" placeholder="Email" type="text" name="email" value="<? echo $_SESSION["prov"]->email; ?>" />
		</div>
	</div>

	<div class="row collapse">
		<div class="medium-4 column">
			<span class="prefix">CDN</span>
		</div>
		<div class="medium-8 column">
			<input required="true" placeholder="CDN" type="text" name="cdn" value="<? echo isset($_SESSION["prov"]->cdn) ? $_SESSION["prov"]->cdn : cdn; ?>" />
		</div>
	</div>
			
	<label><input type="checkbox" name="ssl" value="1" <? echo $_SESSION["prov"]->ssl == 1 ? 'checked' : ''; ?>/> Il sito funziona in solo SSL</label>
	<label><input type="checkbox" name="sendmail" value="smtp" <? echo $_SESSION["prov"]->sendmail == 'smtp' ? 'checked' : ''; ?>/> Invia e-mail con smtp (/mail/_/mail_smtp_conf.lib.php)</label>
	<label><input type="checkbox" name="dev" value="1" <? echo $_SESSION["prov"]->dev == 1 ? 'checked' : ''; ?>/> Abilita modalità sviluppatore (creare ambienti + installare apps)</label>
	<label><input type="checkbox" name="logStack" value="1" <? echo $_SESSION["prov"]->logStack == 1 ? 'checked' : ''; ?>/> Salva lo stack di esecuzione (funziona solo in modalità dev)</label>
	
	<hr />
	<h4>Impostazioni del Database</h4>
	<div class="row collapse">
		<div class="medium-4 column">
			<span class="prefix">Host MySQL</span>
		</div>
		<div class="medium-8 column">
			<input class="" required="true" type="text" name="db_host" value="<? echo isset($_SESSION["db_host"]) ? $_SESSION["db_host"] : localhost; ?>" />
		</div>
	</div>

	<div class="row collapse">
		<div class="medium-4 column">
			<span class="prefix">Username</span>
		</div>
		<div class="medium-8 column">
			<input required="true" type="text" name="db_user" value="<? echo isset($_SESSION["db_user"]) ? $_SESSION["db_user"] : ''; ?>" />
		</div>
	</div>

	<div class="row collapse">
		<div class="medium-4 column">
			<span class="prefix">Password</span>
		</div>
		<div class="medium-8 column">
			<input required="true" type="text" name="db_pass" value="<? echo isset($_SESSION["db_pass"]) ? $_SESSION["db_pass"] : ''; ?>" />
		</div>
	</div>

	<div class="row collapse">
		<div class="medium-4 column">
			<span class="prefix">DB Name</span>
		</div>
		<div class="medium-8 column">
			<input required="true" type="text" name="db_name" value="<? echo isset($_SESSION["db_name"]) ? $_SESSION["db_name"] : ''; ?>" />
		</div>
	</div>

	<div class="row collapse">
		<div class="medium-4 column">
			<span class="prefix">Prefisso Tabelle</span>
		</div>
		<div class="medium-8 column">
			<input type="text" name="db_prefix" value="<? echo isset($_SESSION["db_prefix"]) ? $_SESSION["db_prefix"] : ''; ?>" />
		</div>
	</div>
	<hr />
	
	<h4>Creazione Primo Utente?</h4>
	<div class="row collapse">
		<div class="medium-4 column">
			<span class="prefix">Username</span>
		</div>
		<div class="medium-8 column">
			<input type="text" name="user_username" value="<? echo $_SESSION["user_username"]; ?>" />
		</div>
	</div>
	<div class="row collapse">
		<div class="medium-4 column">
			<span class="prefix">Password</span>
		</div>
		<div class="medium-8 column">
			<input type="text" name="user_password" value="<? echo $_SESSION["user_password"]; ?>" />
		</div>
	</div>
	<div class="alert-box secondary"><p style="margin:0">(Se non vuoi creare un utente base, lascia liberi i campi :)</p></div>

	<input type="submit" class="expand radius button large success" value="Procedi (verifica del db e poi conferma installazione)" style="margin-top: 20px" />
</form>