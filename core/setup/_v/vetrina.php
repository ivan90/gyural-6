<h3>Gyural Showcase</h3>
<hr />
<p>Gyural è giù pronto per essere utilizzato come vetrina.</p>
<p>Modifica il <code>/sys/version.json</code> impostando una <code>IndexApp</code> valida e rimuovi la cartella <strong>/core/setup</strong></p>

<div class="panel">
	<p>Se preferisci, invece che eliminare la cartella <code>/core/setup</code> puoi creare il file <code>/app/setup/_/setup.ctrl.php</code></p>
	<? highlight_file(applicationCore . 'setup/_assets/ctrlExample.php'); ?>
	<p><blockquote>Il metodo <code>->move();</code> è una delle novità di Gyural 1.10, <a href="//gyural.com/docs" target="_blank">scoprile tutte</a>.</blockquote></p>
</div>