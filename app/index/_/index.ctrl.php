<?php

class indexCtrl extends standardController {

	var $index_tollerant = true;
	
	function __construct() {
		$this->ui = LoadClass('index/ui', 1);
	}

	
	
	function GetIndex() {

		$pagina = $_GET["page"];
		if(!$pagina)
			$pagina = 'index';
		
		$pg = str_replace('-', '/', $pagina);
		
		$content = 'app/index/_v/_pages/' . $pg;
		if(!is_file(absolute . $content . '.php'))
			$this->move('index/GetIndex/page:404', array('responseCode' => 404));
		
		if($pg == 404) {
			HttpResponse(404);
		}
		
		$this->ui->__header();
		$this->ui->_render($content);
		$this->ui->__footer();

	}

	/** 
	  * A test class
	  *
	  * @param  foo bar
	  * @return baz
	*/

	function ApiIndex($yourName = 'Fed') {
		return 'Hello ' . $yourName;
	}

	function AjaxName() {
		return 'yah!';
	}

	function myName($env) {
		$env->myName = ucFirst($_GET["name"]);
	}

}

?>