<?

/**
 * GYU - Hooks
 *
 * Event driven hooks!
 * 
 * You can specify an event in every point of the APP in wich other apps could inject is own code.
 * the mechanism is more like an event-listner..
 * 
 * The app trig an event: "common.header".
 *  
 * and from that point on all the listner for the "common.header" will be executed.
 *  
 * Realworld examples:
 *  
 * header.php:
 * <html>
 *  	<head>
 *   		<? \Gyu\Hooks::get('common.html.seo'); ?>
 *   		<link…>
 *   	</head>
 * </html>
 *  
 * /apps/seo/_/seo.hooks.php:
 * \Gyu\Hooks::set('common.html.seo', 'functionName', priority);
 *  
 * /apps/otherapp/_/otherapp.hooks.php:
 * \Gyu\Hooks::set('common.html.seo', 'otherFunctionName', priority);
 *  
 * http://www.rendered-example.com:
 * <html>
 *  	<head>
 *  		<title>My custom title</title>
 *  	 	<description>I'm cool!</description>
 *  	 </head>
 * </html>
 *
 * @version 0.2
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

namespace Gyu;

define('hooks_cache', dev); // hooks_cache

class Hooks {
	/**
	 * Get an hook
	 * 
	 * @param  string $event The name of the event
	 * @param  mixed[] ? Additional args
	 * @return false
	 */
	static function get($event) {

		$args = null;
		if(func_num_args() != 1) {
			$args = func_get_args();
			unset($args[0]);
		}

		$triggers = static::load($event);

		deb_log($event, 'hook');

		if(!is_array($triggers))
			return false;

		foreach($triggers as $trigger) {
			foreach($trigger as $item) {
				deb_log($item, 'hook-event:' . $event);

				if($args)
					call_user_func_array($item, $args);
				else
					call_user_func($item);
			}
		}

	}

	/**
	 * Set an hook
	 * 
	 * @param string $event The event name
	 * @param string $function The function hooked
	 * @param integer $priority Priority of execution
	 * @return false
	 */
	static function set($event, $function, $priority = 0) {
		$GLOBALS["gyu.sys_hooks"][$event][$priority][] = $function;	
	}

	/**
	 * Load the stack of attached events
	 * 
	 * @param  mixed $attr
	 * @return false
	 */
	static function load($attr = null) {

		$listners = static::rebuilt();
		if(is_array($listners))
			foreach($listners as $listner)
				include_once $listner;

		if($attr != null) {
			if(isset($GLOBALS["gyu.sys_hooks"][$attr]))
				return($GLOBALS["gyu.sys_hooks"][$attr]);
			else
				return false;
		} else
			return $GLOBALS["gyu.sys_hooks"];

	}

	/**
	 * Load the list of files to call
	 * 
	 * @return false
	 */
	static function rebuilt() {

		$cacheFile = cache . 'sys/hooks.cache';

		if(is_file($cacheFile) && hooks_cache)
			return unserialize(file_get_contents($cacheFile));

		$applications = deb_installed_app();
		
		foreach($applications as $application) {
			$path = (@$application["isCore"] ? applicationCore : application).$application["name"].'/_/'.$application["name"].'.hooks.php';
			if(is_file($path)) {
				$return[] = $path;
			}
		}
		
		if(hooks_cache) {
			file_put_contents($cacheFile, serialize($return));
		} else
			if(is_file($cacheFile))
				unlink($cacheFile);

		return $return;

	}

	/**
	 * Clear the chace
	 * 
	 * @return false
	 */
	static function trash() {
		unlink(cache . 'sys/hooks.cache');
	}

}